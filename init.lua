minetest.register_node("digiline_wifi:wifi_block", {
	description = "Digiline Wi-Fi block",
	tiles = {"digilinewifi_wifi_block.png"},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	groups = {cracky=1, level=2},
	digiline = {
		receptor = { },
		effector = {
			action = function(pos, _, channel, message)
				local meta = minetest.get_meta(pos)
				if channel == meta:get_string("channel") then
					message = tostring(message)
					digiline.receptor_send(pos, 24, meta:get_string("channel"), message)
				end
			end,
		},
	},
	on_digiline_receive = function(pos, _, channel, sender)
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end,
})

minetest.register_craft({
	output = "digiline_wifi:wifi_block",
	recipe = {
		{"", "technic:control_logic_unit", ""},
		{"digilines:wire_std_00000000", "mesecons_luacontroller:luacontroller0000", "digilines:wire_std_00000000"},
		{"", "digilines:wire_std_00000000", ""}
	}
})
